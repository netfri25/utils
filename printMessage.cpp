#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>

#define EX_CODES_FILE "CodesToMessages.csv"
#define ERROR_MESSAGE_CODE_NOT_FOUND "Error - message code not found"

using CodeToMessage = std::map<std::string_view, std::string>;

std::string readFileToString(const std::string& fileName)
{
	std::ifstream inFile;
	inFile.open(fileName); //open the input file

	std::stringstream strStream;
	strStream << inFile.rdbuf(); //read the file
	std::string str = strStream.str(); //str holds the content of the file

	return str;
}

void clearCommaFromEndOfString(std::string_view& str)
{
	if(str.length() >= 2 && str[str.length() - 2] == ',')
		str = str.substr(0, str.length() - 2);
}

CodeToMessage initializeCodeToMessage()
{
    CodeToMessage codeToMessage;
	std::string fileContent = readFileToString(EX_CODES_FILE);
	std::string_view element;
	std::string_view codeString;
	std::string_view message;
	std::string_view line_delimiter_char = ".\n";
	std::string_view values_delimiter_char = ",";
	size_t pos1 = 0;
	size_t pos2 = 0;
	size_t pos3 = 0;
	int messageCode;

	// prestep remove csv headers
	pos1 = fileContent.find("\n");
	fileContent.erase(0, pos1 + 1);

	while ((pos1 = fileContent.find(line_delimiter_char)) != std::string::npos)
	{
		element = fileContent.substr(0, pos1);
		pos2 = fileContent.find(values_delimiter_char);
		codeString = fileContent.substr(0, pos2);
		pos3 = fileContent.find_first_of('.');
		message = fileContent.substr(pos2 + 1, pos3 - pos2);
		clearCommaFromEndOfString(message);
		codeToMessage.insert(std::pair<std::string, std::string>(codeString, message));
		fileContent.erase(0, pos1 + line_delimiter_char.length());
	}

    return codeToMessage;
}

std::string_view getMessage(std::string_view messageCodeString, const CodeToMessage& codeToMessage)
{
	auto it = codeToMessage.find(messageCodeString);
	if (it == codeToMessage.end())
	{
		return ERROR_MESSAGE_CODE_NOT_FOUND;
	}
	return it->second;
}


int main(int argc, char* argv[])
{
	// check num of arguments
	if (argc != 2)
	{
		std::cerr <<
			"Wrong number of main arguments\n" <<
			"Expected 1 argument - result code\n" <<
			"Usage example: ./Ex1Part1Test 01121" << std::endl;
		exit(3);
	}


    std::string_view messageCodeString = argv[1];
    auto codeToMessage = initializeCodeToMessage();
	auto message = getMessage(messageCodeString, codeToMessage);
	std::cout << message << std::endl;
	if (message == ERROR_MESSAGE_CODE_NOT_FOUND)
		return 1;
	return 0;
}
